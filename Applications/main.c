/**
 * @file 	main.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Applications entry point and basic hardware setup
 */

#include "AppConfig.h"
#include "Clock/rfc868_time_sync.h"
#include "Utils/network_init.h"
#include "SMTPClient/smtpclient.h"
#include "Telnet/telnetd.h"
#include "Utils/usb_cdc_vcom.h"

void soft_reset(void);
static void setup_main_hw(void);
static void send_email_task(void *parameters);

/**
 * Program entry point
 */
int main(void)
{
	/* Reallocate vector table if needed, i.e. started from not 0 address*/
	REALLOCATE_VECTOR_TABLE;

	/* Configure UART and watchdog */
	setup_main_hw();

	/* Configure network stack and start it as user-space daemon */
	static struct s_network_config net;
//	net.ip_addr = "192.168.1.221";
//	net.gateway = "192.168.1.1";
//	net.netmask = "255.255.255.0";
//	net.dns = "192.168.1.1";
	net.use_dhcp = 1; // set to 1 to enable DHCP. Or set to 0 and fill the struct manually
	tcpip_init(network_init, &net);

	/* Start software RTC */
	soft_rtc_start();

	/* First record in log also initialize it */
	syslog_write(LOG_INFORMATION, (signed portCHAR *)"main()", "Started. CPU clock = %lu MHz", SystemCoreClock / 1000000u);

	/* Syncronize the time with internet using RFC868 protocol and set timezone to EEST (UTC+3) */
	sync_time_rfc868(NULL, LOCAL_TIMEZONE);

	/* Send E-Mail @see send_email_task() */
	xTaskCreate(send_email_task, "send email", 400u, NULL, 1,NULL);

	/* Start Telnet daemon */
	telnetd_start();

	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 1;
}

static void send_email_task(void *parameters)
{
	struct s_email_message mes;

	/* XXX Fill this struct with our info */
	mes.from_email = "sender@server.domain";
	mes.from_name = "Vasya Pupkin";
	mes.login = "v.pupkin";
	mes.password = "1234";
	mes.server = "smtp.server.domain"; // the server must support AUTH LOGIN without SSL
	mes.subj = "Hello, world!";
	mes.message = "I am demo SMTP client on LPC176x with FreeRTOS and LwIP";
	mes.to_cc[0] = "recevier@server.domain";
	mes.to_cc[1] = "oleg.b.antonyan@gmail.com"; // send me a copy :)
	mes.to_cc[2] = NULL; // last address must be NULL

	/* and uncomment this when you are done */
	//smtp_send(&mes);

	vTaskDelete(NULL);
}

void vApplicationStackOverflowHook( xTaskHandle pxTask, signed char *pcTaskName )
{
	/* Bad thing =( Print some useful info and reboot */
	TSPRINTF("Stack overflow in %s\n", pcTaskName);
	/* The is a chance that syslog is still working */
	syslog_write(LOG_ERROR, pcTaskName, "Stack overflow");
	soft_reset();
}

void HardFault_Handler(void)
{
	/* Very bad thing =( Print some useful info and reboot */
	TSPRINTF("HardFault CFSR = %X  BFAR = %X\n", SCB->CFSR, SCB->BFAR);
	soft_reset();
}

void vApplicationIdleHook(void)
{
	/* Feed the dog in the idle hook task. Not the best way to do this, but this is just example */
	WDT_Feed();
}

/**
 * Reset CPU using watchdog
 */
void soft_reset(void)
{
	__disable_irq();
	__disable_fault_irq();
	volatile uint_fast8_t fuck_optimization = 0;
	while(1) // wait until wdt overflows
	{
		fuck_optimization++; // ...and bark after WDT_RESET_TIMEOUT
	}
}

/**
 * Setup UART0 and WDT. Probably other hardware
 */
static void setup_main_hw(void)
{
	/* Enable UART0 */
	PINSEL_CFG_Type PinCfg;
	UART_CFG_Type uartCfg;
	UART_FIFO_CFG_Type uartFifoCfg;

	PinCfg.Funcnum = PINSEL_FUNC_1;
	PinCfg.Pinnum = PINSEL_PIN_2;
	PinCfg.Portnum = PINSEL_PORT_0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = PINSEL_PIN_3;
	PINSEL_ConfigPin(&PinCfg);

	UART_ConfigStructInit(&uartCfg);
	uartCfg.Baud_rate = UART0_BAUDRATE;
	uartCfg.Databits = UART_DATABIT_8;
	uartCfg.Parity = UART_PARITY_NONE;
	uartCfg.Stopbits = UART_STOPBIT_1;

	UART_Init((LPC_UART_TypeDef *)LPC_UART0, &uartCfg);

	UART_FIFOConfigStructInit(&uartFifoCfg);
	UART_FIFOConfig((LPC_UART_TypeDef *) LPC_UART0, &uartFifoCfg);

	UART_TxCmd((LPC_UART_TypeDef *) LPC_UART0, ENABLE); // Enable UART Transmit
	UART_IntConfig((LPC_UART_TypeDef *)LPC_UART0, UART_INTCFG_RBR, ENABLE);  // Enable UART Rx interrupt

	NVIC_SetPriority(UART0_IRQn, 10u);
	/* Enable UART0 interrupt whenever you want */
	//NVIC_EnableIRQ(UART0_IRQn);

#ifdef APPCONFIG_DO_YOU_LIKE_DOGS
	/* Enable watchdog*/
	WDT_Init(WDT_CLKSRC_IRC, WDT_MODE_RESET); // WDT from internal RC clock and reset enable
	WDT_Start(WDT_RESET_TIMEOUT);
#endif

	/* Enable UART3 */
	PinCfg.Funcnum = PINSEL_FUNC_2;
	PinCfg.Pinnum = PINSEL_PIN_0;
	PinCfg.Portnum = PINSEL_PORT_0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = PINSEL_PIN_1;
	PINSEL_ConfigPin(&PinCfg);

	UART_ConfigStructInit(&uartCfg);
	uartCfg.Baud_rate = UART3_BAUDRATE;
	uartCfg.Databits = UART_DATABIT_8;
	uartCfg.Parity = UART_PARITY_NONE;
	uartCfg.Stopbits = UART_STOPBIT_1;

	UART_Init((LPC_UART_TypeDef *)LPC_UART3, &uartCfg);

	UART_FIFOConfigStructInit(&uartFifoCfg);
	UART_FIFOConfig((LPC_UART_TypeDef *) LPC_UART3, &uartFifoCfg);

	UART_TxCmd((LPC_UART_TypeDef *) LPC_UART3, ENABLE); // Enable UART Transmit
	UART_IntConfig((LPC_UART_TypeDef *)LPC_UART3, UART_INTCFG_RBR, ENABLE);  // Enable UART Rx interrupt

	NVIC_SetPriority(UART3_IRQn, 10u);
	/* Enable UART3 interrupt whenever you want */
	//NVIC_EnableIRQ(UART#_IRQn);
}
