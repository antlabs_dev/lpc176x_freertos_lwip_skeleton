/**
 * @file 	at45db081d_driver.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Driver for AT45DB081D dataflash using hardware SSP0 or SSP1 in LPC176x
 * Based on code from Embedded Artist LPCXpresso BaseBoard
 * http://www.embeddedartists.com/products/lpcxpresso/xpr_base.php
 * Original license note:
 * *****************************************************************************
 *   flash.c:  Driver for the SPI Flash (AT45DB081D-SU)
 *
 *   Copyright(C) 2009, Embedded Artists AB
 *   All rights reserved.
 *
 ******************************************************************************
 *
 */

#ifndef __AT45DB081D_DRIVER_H
#define __AT45DB081D_DRIVER_H

#include "AppConfig.h"

#define AT45DB0801D_SSP_HARDWARE	SSP0 //!< either SSP0 or SSP1 of LPC176x
#define AT45DB0801D_SSP_USE_SOFTWARE_SSEL //!< if we going to use software chip select. check the GPIO number for this

/**
 * Write data to flash
 * @param buf - pointer to buffer written
 * @param offset - absolute address in flash
 * @param len - size of buffer to write
 * @return number of bytes written
 */
size_t at45db081d_write(const uint8_t *buf, size_t offset, size_t len);

/**
 * Read data from flash
 * @param buf - pointer to read buffer
 * @param offset - absolute address in flash
 * @param len - size of buffer to read
 * @return number of bytes read
 */
size_t at45db081d_read(uint8_t *buf, size_t offset, size_t len);

/**
 * Switch dataflash to 256-bytes page size instead original 264
 */
void at45db081d_set_to_binary_page_size(void);

/**
 * Return current flash page size
 * @return isn't it obvious?
 */
uint16_t at45db081d_get_page_size(void);

#endif /* end __AT45DB081D_DRIVER_H */
