/**
 * @file 	flash_hal.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Few functions used to thread-safe lash access
 */

#include "flash_hal.h"

#define CHECK_INIT_FALSH_GATEKEEPER() \
	if(!initialized) \
	{ initialized = flash_gatekeeper_init();	}

typedef enum { FLASH_HAL_WRITE, FLASH_HAL_READ } FlashHalRequestType;

static uint8_t initialized = 0;

static xSemaphoreHandle sema = NULL;

static uint_fast8_t flash_gatekeeper_init(void);
static uint_fast8_t flash_rw(size_t page_offset, void *buffer, size_t length, uint32_t block_wait_ms, FlashHalRequestType type);

uint_fast8_t flash_write(size_t addr_offset, const void *buffer, size_t length, uint32_t block_wait_ms)
{
	CHECK_INIT_FALSH_GATEKEEPER();
	return flash_rw(addr_offset, (void *)buffer, length, block_wait_ms, FLASH_HAL_WRITE);
}

uint_fast8_t flash_read(size_t addr_offset, void *buffer, size_t length, uint32_t block_wait_ms)
{
	CHECK_INIT_FALSH_GATEKEEPER();
	return flash_rw(addr_offset, buffer, length, block_wait_ms, FLASH_HAL_READ);
}

static uint_fast8_t flash_rw(size_t page_offset, void *buffer, size_t length, uint32_t block_wait_ms, FlashHalRequestType type)
{
	if(xSemaphoreTake(sema, block_wait_ms / portTICK_RATE_MS) != pdPASS)
	{
		return 0;
	}
	size_t result = 0;
	switch(type)
	{
		case FLASH_HAL_WRITE:
			if(length > at45db081d_get_page_size())
			{
				break;
			}
			result = at45db081d_write((const uint8_t *)buffer, page_offset, length);
			break;

		case FLASH_HAL_READ:
			result = at45db081d_read((uint8_t *)buffer, page_offset, length);
			break;
		default:
			break;
	}
	xSemaphoreGive(sema);
	if(result != length)
	{
		return 0;
	}
	return 1u;
}

static uint_fast8_t flash_gatekeeper_init(void)
{
	sema = xSemaphoreCreateMutex();
	if(!sema)
	{
		return 0;
	}
	return 1u;
}
