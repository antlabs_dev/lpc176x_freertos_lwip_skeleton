/**
 * @file 	at45db081d_driver.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Driver for AT45DB081D dataflash using hardware SSP0 or SSP1 in LPC176x
 * Based on code from Embedded Artist LPCXpresso BaseBoard
 * http://www.embeddedartists.com/products/lpcxpresso/xpr_base.php
 * Original license note:
 * *****************************************************************************
 *   flash.c:  Driver for the SPI Flash (AT45DB081D-SU)
 *
 *   Copyright(C) 2009, Embedded Artists AB
 *   All rights reserved.
 *
 ******************************************************************************
 *
 */

#include "at45db081d_driver.h"

#ifndef MIN
	#define MIN(x, y) ((x) < (y) ? (x) : (y))
#endif

#define CHECK_INIT_AT45DB081D() \
	if(!initialized) \
	{ initialized = flash_init();	}

#define FLASH_CS_OFF() software_ssel(1u)
#define FLASH_CS_ON()  software_ssel(0)

#define FLASH_CMD_RDID      0x9F        /* read device ID */
#define FLASH_CMD_RDSR      0xD7        /* read status register */
#define FLASH_CMD_FAST_READ 0x0B        /* read data (extra command setup time allows higher SPI clock) */

#define FLASH_CMD_SE        0x7C        /* sector erase */
#define FLASH_CMD_BE        0x50        /* block erase */
#define FLASH_CMD_PE        0x81        /* page erase */
#define FLASH_CMD_PP_BUF    0x82        /* page program through buffer 1 */

#define FLASH_CMD_DP        0xB9        /* deep power down */
#define FLASH_CMD_RES       0xAB        /* release from deep power down */

#define ATMEL_MANU_ID  		0x1F
#define ATMEL_DEVICE_ID1 ((1 << 5u) | (0x05) ) /* DataFlash, 8 MBit */

/* status register defines */
#define STATUS_RDY      (1u << 7u)
#define STATUS_COMP     (1u << 6u)
#define STATUS_PROTECT  (1u << 1u)
#define STATUS_POW2     (1u << 0)

#define FLAG_IS_POW2 0x01

struct s_flash_info {
    const char *name;
    uint32_t jedec_id;
    uint32_t nr_pages;
    uint16_t pagesize;
    uint16_t pageoffset;
    uint8_t flags;
};

static uint16_t page_size = 0;
static uint16_t page_offset = 0;
static uint8_t page_is_pow2 = 0;
static uint8_t page_size_changed = 0;
static size_t flash_total_size = 0;
static LPC_SSP_TypeDef *ssp_device;
static uint8_t initialized = 0;

static struct s_flash_info flash_devices[] = {
        {"AT45DB081D", 0x1F2500, 4096, 264, 9, 0},
        {"AT45DB081D", 0x1F2500, 4096, 256, 8, FLAG_IS_POW2},
        {"AT45DB081D", 0x1F2600, 4096, 528, 10, 0},
        {"AT45DB081D", 0x1F2600, 4096, 512, 9, FLAG_IS_POW2},
};

static void ssp_send(const uint8_t *buf, size_t length);
static void ssp_receive(uint8_t *buf, size_t length);
static void exit_deep_powerdown(void);
static void read_deviceid(uint8_t *deviceid);
static uint8_t read_status(void);
static void poll_is_busy(void);
static void set_address_bytes(uint8_t *addr, size_t offset);
static void software_ssel(uint8_t cs);
static uint_fast8_t flash_init(void);
static void ssp_init(void);

/******************************************************************************
 *
 * Description:
 *    Write data to flash
 *
 * Params:
 *   [in] buf - data to write to flash
 *   [in] offset - offset into the flash
 *   [in] len - number of bytes to write
 *
 * Returns:
 *   number of written bytes
 *
 *****************************************************************************/
size_t at45db081d_write(const uint8_t *buf, size_t offset, size_t len)
{
	CHECK_INIT_AT45DB081D();

    uint16_t wLen;
    uint32_t written = 0;
    uint8_t addr[4];

    if(len > flash_total_size || (len + offset) > flash_total_size)
    {
        return 0;
    }

    if(page_size_changed)
    {
        return 0;
    }

    /* write up to first page boundry */
    wLen = (((((offset % page_size) / page_size) + 1u) * page_size) - (offset % page_size));
    wLen = MIN(wLen, len);

    while(len)
    {

        /* write address */
        addr[0] = FLASH_CMD_PP_BUF;
        set_address_bytes(&addr[1u], offset);

        FLASH_CS_ON();

        ssp_send(addr, 4u);

        /* write data */
        ssp_send(&buf[written], wLen);

        FLASH_CS_OFF();

        /* delay to wait for a write cycle */
        //eepromDelay();

        len     -= wLen;
        written += wLen;
        offset  += wLen;
        wLen = MIN(page_size, len);

        poll_is_busy();
    }

    return written;
}

/******************************************************************************
 *
 * Description:
 *    Read data from flash
 *
 * Params:
 *   [in] buf - data buffer
 *   [in] offset - offset into the flash
 *   [in] len - number of bytes to read
 *
 * Returns:
 *   number of read bytes
 *
 *****************************************************************************/
size_t at45db081d_read(uint8_t *buf, size_t offset, size_t len)
{
	CHECK_INIT_AT45DB081D();

    uint8_t addr[5] = {0};

    if(len > flash_total_size || len+offset > flash_total_size)
    {
        return 0;
    }

    if(page_size_changed)
    {
        return 0;
    }

    addr[0] = FLASH_CMD_FAST_READ;

    set_address_bytes(&addr[1], offset);
    addr[4u] = (0);

    FLASH_CS_ON();

    ssp_send(addr, 5u);
    ssp_receive(buf, len);

    FLASH_CS_OFF();

    return len;
}

/******************************************************************************
 *
 * Description:
 *    Get flash page size
 *
 * Returns:
 *   256 or 264
 *
 *****************************************************************************/
uint16_t at45db081d_get_page_size(void)
{
	CHECK_INIT_AT45DB081D();
    return page_size;
}

/******************************************************************************
 *
 * Description:
 *    Change page size to binary page, i.e. 256 bytes.
 *
 *    NOTE: This operation cannot be undone and requires a power-cycle
 *    before taking effect.
 *
 *****************************************************************************/
void at45db081d_set_to_binary_page_size(void)
{
	CHECK_INIT_AT45DB081D();

    const uint8_t data[4] = {0x3D, 0x2A,  0x80, 0xA6};

    FLASH_CS_ON();

    ssp_send(data, 4u);

    FLASH_CS_OFF();

    page_size_changed = (!page_is_pow2);
}

static void ssp_send(const uint8_t *buf, size_t length)
{
    SSP_DATA_SETUP_Type xferConfig;

	xferConfig.tx_data = (uint8_t *)buf;
	xferConfig.rx_data = NULL;
	xferConfig.length  = length;

    SSP_ReadWrite(ssp_device, &xferConfig, SSP_TRANSFER_POLLING);
}

static void ssp_receive(uint8_t *buf, size_t length)
{
    SSP_DATA_SETUP_Type xferConfig;

	xferConfig.tx_data = NULL;
	xferConfig.rx_data = buf;
	xferConfig.length  = length;

    SSP_ReadWrite(ssp_device, &xferConfig, SSP_TRANSFER_POLLING);
}

static void exit_deep_powerdown(void)
{
    uint8_t cmd = FLASH_CMD_RES;
    FLASH_CS_ON();

    ssp_send( (uint8_t *)&cmd, 1 );

    FLASH_CS_OFF();
}

static void read_deviceid(uint8_t *deviceid)
{
    uint8_t cmd = FLASH_CMD_RDID;

    FLASH_CS_ON();

    ssp_send((uint8_t *)&cmd, 1);
    ssp_receive(deviceid, 4);

    FLASH_CS_OFF();
}

static uint8_t read_status(void)
{
    uint8_t cmd = FLASH_CMD_RDSR;
    uint8_t status = 0;

    FLASH_CS_ON();

    ssp_send( (uint8_t *)&cmd, 1 );
    ssp_receive( &status, 1 );

    FLASH_CS_OFF();

    return status;
}


static void poll_is_busy(void)
{
  uint8_t status = 0;
  int i = 0;

  do
  {
    for (i = 0; i < 0x2000; i++);

    status = read_status();
  }
  while ((status & STATUS_RDY) == 0);
}

static void set_address_bytes(uint8_t *addr, size_t offset)
{
    if(!page_is_pow2)
    {
        uint16_t page = offset / page_size;
        uint16_t off  = offset % page_size;

        /* buffer address bits */
        addr[2u] = (off & 0xFF);
        addr[1u] = (off / page_size);

        /* page address bits */
        addr[1u] |= ((page & ((1u << (16u - page_offset)) - 1u)) << (page_offset - 8u));
        addr[0] = ((page >> (16u - page_offset)) & ((1u << (16u - page_offset)) - 1u) );
    }
    else
    {

        addr[0] = ((offset >> 16u) & 0xFF);
        addr[1u] = ((offset >>  8u) & 0xFF);
        addr[2u] = ((offset >>  0u) & 0xFF);

    }
}

/******************************************************************************
 *
 * Description:
 *    Initialize the SPI Flash Driver
 *
 * Returns:
 *   TRUE if initialization successful, otherwise FALSE
 *
 *****************************************************************************/
static uint_fast8_t flash_init(void)
{
    uint8_t deviceId[4] = {0}, status = 0, flag = 0;
    uint32_t id = 0, i = 0;

    ssp_init();

    FLASH_CS_OFF();

    exit_deep_powerdown();

    read_deviceid(deviceId);

    do
    {
        status = read_status();
    } while((status & STATUS_RDY) == 0);

    if((status & STATUS_POW2) != 0)
    {
        flag = FLAG_IS_POW2;
    }

    id = (deviceId[0] << 16 | deviceId[1] << 8 | deviceId[2]);

    for (i = 0; i < sizeof(flash_devices) / sizeof(struct s_flash_info); i++)
    {
        if (flash_devices[i].jedec_id == id && flash_devices[i].flags == flag)
        {
            page_size   = flash_devices[i].pagesize;
            page_offset = flash_devices[i].pageoffset;;
            page_is_pow2 = ((flash_devices[i].flags & FLAG_IS_POW2) != 0) ;
            flash_total_size = flash_devices[i].pagesize * flash_devices[i].nr_pages;

            return 1u;
        }
    }

    return 0;
}

static void software_ssel(uint8_t cs)
{
#ifndef AT45DB0801D_SSP_USE_SOFTWARE_SSEL
	return;
#else
	#if (AT45DB0801D_SSP_HARDWARE == SSP0)
		(cs == 0 ? GPIO_ClearValue : GPIO_SetValue)(0, 1u << 16u);
	#elif (AT45DB0801D_SSP_HARDWARE == SSP1)
		(cs == 0 ? GPIO_ClearValue : GPIO_SetValue)(0, 1u << 6u);
	#endif
#endif
}

static void ssp_init(void)
{
	SSP_CFG_Type SSP_ConfigStruct;
	PINSEL_CFG_Type PinCfg;

	/* Initialize SPI pin connect
	 * For SSP0:
	 * P0.15 - SCK;
	 * P0.17 - MISO
	 * P0.18 - MOSI
	 * P0.16 - SSEL - either GPIO or SSEL0
	 *
	 * For SSP1:
	 * P0.7 - SCK;
	 * P0.8 - MISO
	 * P0.9 - MOSI
	 * P0.6 - SSEL -  either GPIO or SSEL1
	 */
#if (AT45DB0801D_SSP_HARDWARE == SSP0)
	ssp_device = LPC_SSP0;
	PinCfg.Funcnum = PINSEL_FUNC_2;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum = PINSEL_PORT_0;
	PinCfg.Pinnum = PINSEL_PIN_15;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	PinCfg.Pinnum = PINSEL_PIN_17;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	PinCfg.Pinnum = PINSEL_PIN_18;
	PINSEL_ConfigPin(&PinCfg);

#ifdef AT45DB0801D_SSP_USE_SOFTWARE_SSEL
	PinCfg.Funcnum = PINSEL_FUNC_0;
	PinCfg.Pinnum = PINSEL_PIN_16;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir(0, 1u << 16u , 1u);
#else
	PinCfg.Pinnum = PINSEL_PIN_16;
	PINSEL_ConfigPin(&PinCfg);
#endif
#elif (AT45DB0801D_SSP_HARDWARE == SSP1)
	ssp_device = LPC_SSP1;
	PinCfg.Funcnum = PINSEL_FUNC_2;
	PinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	PinCfg.Pinmode = PINSEL_PINMODE_PULLUP;
	PinCfg.Portnum = PINSEL_PORT_0;
	PinCfg.Pinnum = PINSEL_PIN_7;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = PINSEL_PIN_8;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = PINSEL_PIN_9;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Funcnum = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 2;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir(2, 1<<2, 1);
	GPIO_ClearValue( 2, 1<<2 );
#ifdef AT45DB0801D_SSP_USE_SOFTWARE_SSEL
	PinCfg.Funcnum = PINSEL_FUNC_0;
	PinCfg.Pinnum = PINSEL_PIN_6;
	PINSEL_ConfigPin(&PinCfg);
	GPIO_SetDir(0, 1u << 6u , 1u);
#else
	PinCfg.Pinnum = PINSEL_PIN_6;
	PINSEL_ConfigPin(&PinCfg);
#endif
#endif

	SSP_ConfigStructInit(&SSP_ConfigStruct);
	// Initialize SSP peripheral with parameter given in structure above
	SSP_Init(ssp_device, &SSP_ConfigStruct);
	// Enable SSP peripheral
	SSP_Cmd(ssp_device, ENABLE);
}
