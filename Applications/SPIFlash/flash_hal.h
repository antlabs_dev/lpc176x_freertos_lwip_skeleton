/**
 * @file 	flash_hal.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Few functions used to thread-safe lash access
 */

#ifndef FLASH_HAL_H_
#define FLASH_HAL_H_

#include "AppConfig.h"
#include "at45db081d_driver.h"

/**
 * Write data to flash
 * @param addr_offset - absolute address in flash where data must be written
 * @param buffer - pointer to buffer
 * @param length - size of data to be written in bytes
 * @param block_wait_ms - time in mS how long to wait mutex if it is not free
 * @return 1 if data written successfully, 0 otherwise
 */
uint_fast8_t flash_write(size_t addr_offset, const void *buffer, size_t length, uint32_t block_wait_ms);

/**
 * Read data from flash
 * @param addr_offset - absolute address in flash from where data has to be read
 * @param buffer - pointer to read buffer in RAM
 * @param length - size of data to be read in bytes
 * @param block_wait_ms - time in mS how long to wait mutex if it is not free
 * @return 1 if data read successfully, 0 otherwise
 */
uint_fast8_t flash_read(size_t addr_offset, void *buffer, size_t length, uint32_t block_wait_ms);

#endif /* FLASH_HAL_H_ */
