/**
 * @file 	format_time.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Return formated string with current (or uptime) time representation
 */

#ifndef FORMATTIME_H_
#define FORMATTIME_H_

#include "AppConfig.h"
#include "time.h"

enum {
	DEFAULT_TIME_FORMAT,
	POSIX_TIME_FORMAT,
	LOG_FILE_NAME_TIME_FORMAT_TIME_FORMAT,
	MCDI_TIME_FORMAT,
	SURGARD_TIME_FORMAT,
	UPTIME_TIME_FORMAT
};

/**
 * Public interface - get formatted time
 * @param format - format of string time
 * @param buffer - buffer to which string will be eritten
 * @param size - size of buffer allocated
 * @return 1 if OK, 0 if error occurred
 */

uint_fast8_t get_string_time(uint_fast8_t format, char *buffer, size_t size);

/**
 * Public interface - get uptime in fuman gedable format
 * @param buffer - buffer where string will be written
 * @param size - size of buffer
 * @return 1 if OK, 0 if error occured
 */
uint_fast8_t get_string_uptime(char *buffer, size_t size);

#endif /* FORMATTIME_H_ */
