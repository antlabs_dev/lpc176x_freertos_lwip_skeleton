/**
 * @file 	smtpclient.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Simple client to send E-Mails
 */

#include "smtpclient.h"

static uint_fast8_t encode_base64(size_t s_len, const char *src, size_t d_len, uint8_t *dst);
static uint_fast8_t iconv_cp1251_to_unicode(const uint8_t *src, uint8_t *dst, size_t ds_leght);

#define SMTP_SEND_CRLF() send(sockfd, smtp_crnl, strlen(smtp_crnl), 0)
#define SMTP_DUMMY_READ() recv(sockfd, buf, sizeof buf, 0)

uint_fast8_t smtp_send(const struct s_email_message *mes)
{
	const char *smtp_220 = "220";
	const char *smtp_helo = "HELO ";
	const char *smtp_authlogin = "AUTH LOGIN";
	const char *smtp_mail_from = "MAIL FROM: ";
	const char *smtp_rcpt_to = "RCPT TO: ";
	const char *smtp_data = "DATA\r\n";
	const char *smtp_to ="To: ";
	const char *smtp_cc = "Cc: ";
	const char *smtp_from = "From: ";
	const char *smtp_subject = "Subject: ";
	const char *smtp_quit = "QUIT\r\n";
	const char *smtp_crnl = "\r\n";
	const char *smtp_content_type = "Content-Type: text/plain; charset=utf-8";
	const char *smtp_mime_version = "MIME-Version: 1.0";
	const char *smtp_transfer_encoding = "Content-Transfer-Encoding: 8bit";
	const char *smtp_crnlperiodcrnl = "\r\n.\r\n";

	const char *syslog_thread_name = "smtpclient";
	const char *localhost_name = "lpc176x_freertos_lwip_skeleton";

	const uint_fast16_t ack_wait_timeout = SMTP_BLOCKING_READ_TIMEOUT_SECONDS * 1000u; 	/* Time to wait blocking read operation */

	uint8_t login_base64[32u] = {0};
	uint8_t password_base64[32u] = {0};
	encode_base64(strlen(mes->login), mes->login, sizeof login_base64, login_base64);
	encode_base64(strlen(mes->password), mes->password, sizeof password_base64, password_base64);

	syslog_write(LOG_INFORMATION, syslog_thread_name, "Started");

	struct sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof serv_addr);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(25u);

	/* Validate string host whether it is IP address or domain name */
	ip_addr_t temp;
	uint8_t use_dns = !inet_aton(mes->server, &temp);

	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Cannot create socket. %d", sockfd);
		return 0;
	}
	else
	{
		if(use_dns)
		{
			struct hostent *hp_result, h_hp;
			char buf[64] = {0};
			int err = -1;

			if(gethostbyname_r(mes->server, &h_hp, buf, sizeof buf, &hp_result, &err) == 0)
			{
				syslog_write(LOG_INFORMATION, syslog_thread_name, "Resolved %s is %s", hp_result->h_name,
						inet_ntoa(*(struct in_addr *)(hp_result->h_addr_list[0])));
				serv_addr.sin_addr = *(struct in_addr *)(hp_result->h_addr_list[0]);
			}
			else
			{
				syslog_write(LOG_INFORMATION, syslog_thread_name, "Cannot resolve %s, error %d", hp_result->h_name, err);
				close(sockfd);
				return 0;
			}
		}
		else
		{
			serv_addr.sin_addr.s_addr = inet_addr(mes->server);
		}
		setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &ack_wait_timeout, sizeof(ack_wait_timeout));
	}

	char buf[64] = {0}; 

	if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Cannot connect to %s:%u", \
				inet_ntoa(serv_addr.sin_addr.s_addr), ntohs(serv_addr.sin_port));
		close(sockfd);
		return 0;
	}

	if(recv(sockfd, buf, sizeof buf, 0) <= 0)
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Cannot receive data");
		close(sockfd);
		return 0;
	}
	if(strncmp(smtp_220, buf, strlen(smtp_220)) != 0)
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Wrong server reply after connection: %s", buf);
		close(sockfd);
		return 0;
	}
	send(sockfd, smtp_helo, strlen(smtp_helo), 0);
	send(sockfd, localhost_name, strlen(localhost_name), 0);
	SMTP_SEND_CRLF();

	memset(buf, 0, sizeof buf);
	if(recv(sockfd, buf, sizeof buf, 0) <= 0)
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Cannot receive data");
		close(sockfd);
		return 0;
	}
	if(buf[0] != '2')
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Wrong server reply for %s: %s", smtp_helo, buf);
		close(sockfd);
	}

	send(sockfd, smtp_authlogin, strlen(smtp_authlogin), 0);
	SMTP_SEND_CRLF();
	SMTP_DUMMY_READ();

	send(sockfd, login_base64, strlen((const char *)login_base64), 0); // login in base64
	SMTP_SEND_CRLF();
	SMTP_DUMMY_READ();

	send(sockfd, password_base64, strlen((const char *)password_base64), 0); // password in base64
	SMTP_SEND_CRLF();
	SMTP_DUMMY_READ();

	send(sockfd, smtp_mail_from, strlen(smtp_mail_from), 0);
	send(sockfd, mes->from_email, strlen(mes->from_email), 0);
	SMTP_SEND_CRLF();

	memset(buf, 0, sizeof buf);
	if(recv(sockfd, buf, sizeof buf, 0) <= 0)
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Cannot receive data");
		close(sockfd);
		return 0;
	}
	if(buf[0] != '2')
	{
		syslog_write(LOG_ERROR, pcTaskGetTaskName(NULL), "Wrong server reply for %s: %s", smtp_mail_from, buf);
		close(sockfd);
		return 0;
	}

	uint8_t i = 0;
	while(NULL != mes->to_cc[i])
	{
		send(sockfd, smtp_rcpt_to, strlen(smtp_rcpt_to), 0);
		send(sockfd, mes->to_cc[i], strlen(mes->to_cc[i]), 0);
		SMTP_SEND_CRLF();

		memset(buf, 0, sizeof buf);
		if(recv(sockfd, buf, sizeof buf, 0) <= 0)
		{
			syslog_write(LOG_ERROR, syslog_thread_name, "Cannot receive data");
			close(sockfd);
			return 0;
		}
		if(buf[0] != '2')
		{
			syslog_write(LOG_ERROR, syslog_thread_name, "Wrong server reply for %s: %s", smtp_rcpt_to, buf);
			close(sockfd);
			return 0;
		}
		i++;
	}

	send(sockfd, smtp_data, strlen(smtp_data), 0);

	memset(buf, 0, sizeof buf);
	if(recv(sockfd, buf, sizeof buf, 0) <= 0)
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Cannot receive data");
		close(sockfd);
		return 0;
	}
	if(buf[0] != '3')
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Wrong server reply for %s: %s", smtp_data, buf);
		close(sockfd);
		return 0;
	}
	send(sockfd, smtp_to, strlen(smtp_to), 0);
	if(NULL !=  mes->to_cc[0])
	{
		send(sockfd, mes->to_cc[0], strlen(mes->to_cc[0]), 0);
		SMTP_SEND_CRLF();
	}

	i = 1u;
	while(NULL !=  mes->to_cc[i])
	{
		send(sockfd, smtp_cc, strlen(smtp_cc), 0);
		send(sockfd, mes->to_cc[i], strlen(mes->to_cc[i]), 0);
		SMTP_SEND_CRLF();
		i++;
	}

	memset(buf, 0, sizeof buf);
	strcat(buf, mes->from_name);
	strcat(buf, " <");
	strcat(buf, mes->from_email);
	strcat(buf, ">");
	send(sockfd, smtp_from, strlen(smtp_from), 0);
	send(sockfd, buf, strlen(buf), 0);
	SMTP_SEND_CRLF();

	send(sockfd, smtp_subject, strlen(smtp_subject), 0);
	send(sockfd, mes->subj, strlen(mes->subj), 0);
	SMTP_SEND_CRLF();

	send(sockfd, smtp_content_type, strlen(smtp_content_type), 0);
	SMTP_SEND_CRLF();

	send(sockfd, smtp_mime_version, strlen(smtp_mime_version), 0);
	SMTP_SEND_CRLF();

	send(sockfd, smtp_transfer_encoding, strlen(smtp_transfer_encoding), 0);
	SMTP_SEND_CRLF();

	send(sockfd, mes->message, strlen(mes->message), 0);
	send(sockfd, smtp_crnlperiodcrnl, strlen(smtp_crnlperiodcrnl), 0);

	memset(buf, 0, sizeof buf);
	if(recv(sockfd, buf, sizeof buf, 0) <= 0)
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Cannot receive data");
		close(sockfd);
		return 0;
	}
	if(buf[0] != '2')
	{
		syslog_write(LOG_ERROR, syslog_thread_name, "Wrong server reply for data: %s", buf);
		close(sockfd);
		return 0;
	}

	send(sockfd, smtp_quit, strlen(smtp_quit), 0);

	syslog_write(LOG_INFORMATION, syslog_thread_name, "E-Mail message sent successfully");

	shutdown(sockfd, SHUT_RDWR);
	close(sockfd);
	return 1u;
}

static uint_fast8_t encode_base64(size_t s_len, const char *src, size_t d_len, uint8_t *dst)
{
	const uint8_t base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	uint32_t triad = 0;
	for(triad = 0; triad < s_len; triad += 3u)
	{
		uint32_t sr = 0, byte = 0;
		for(byte = 0; (byte < 3) && (triad + byte < s_len); ++byte)
		{
			sr <<= 8u;
			sr |= (*(src + triad + byte) & 0xFF);
		}

		sr <<= (6u - ((8u * byte) % 6u)) % 6u; /*shift left to next 6bit alignment*/
		if(d_len < 4u)
		{
			return 0; /* error - dest too short */
		}

		*(dst + 0) = *(dst + 1u) = *(dst + 2u) = *(dst + 3u) = '=';
		switch(byte)
		{
		case 3u:
			*(dst + 3u) = base64[sr & 0x3F];
			sr >>= 6u;
		case 2u:
			*(dst + 2u) = base64[sr & 0x3F];
			sr >>= 6;
		case 1u:
			*(dst + 1) = base64[sr & 0x3F];
			sr >>= 6u;
			*(dst + 0) = base64[sr & 0x3F];
		}
		dst += 4u;
		d_len -= 4u;
	}
	return 1u;
}

static uint32_t unichar_to_utf8(uint32_t c, uint8_t *outbuf)
{
	uint32_t len = 0, first = 0, i = 0;

	if(c < 0x80)
	{
		first = 0u;
		len = 1u;
	}
	else if(c < 0x800)
	{
		first = 0xC0;
		len = 2u;
	}
	else if(c < 0x10000)
	{
		first = 0xE0;
		len = 3u;
	}
	else if(c < 0x200000)
	{
		first = 0xF0;
		len = 4u;
	}
	else if(c < 0x4000000)
	{
		first = 0xF8;
		len = 5u;
	}
	else
	{
		first = 0xFC;
		len = 6u;
	}

	for (i = len - 1; i > 0; --i)
	{
		outbuf[i] = (c & 0x3F) | 0x80;
		c >>= 6u;
	}
	outbuf[0] = c | first;

	return len;
}

static uint_fast8_t iconv_cp1251_to_unicode(const uint8_t *src, uint8_t *dst, size_t ds_leght)
{
	const uint16_t win_cp1251[] = {
		0x0402, 0x0403, 0x201A, 0x0453, 0x201E, 0x2026, 0x2020, 0x2021,
		0x20AC, 0x2030, 0x0409, 0x2039, 0x040A, 0x040C, 0x040B, 0x040F,
		0x0452, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014,
		0xFFFD, 0x2122, 0x0459, 0x203A, 0x045A, 0x045C, 0x045B, 0x045F,
		0x00A0, 0x040E, 0x045E, 0x0408, 0x00A4, 0x0490, 0x00A6, 0x00A7,
		0x0401, 0x00A9, 0x0404, 0x00AB, 0x00AC, 0x00AD, 0x00AE, 0x0407,
		0x00B0, 0x00B1, 0x0406, 0x0456, 0x0491, 0x00B5, 0x00B6, 0x00B7,
		0x0451, 0x2116, 0x0454, 0x00BB, 0x0458, 0x0405, 0x0455, 0x0457,
		0x0410, 0x0411, 0x0412, 0x0413, 0x0414, 0x0415, 0x0416, 0x0417,
		0x0418, 0x0419, 0x041A, 0x041B, 0x041C, 0x041D, 0x041E, 0x041F,
		0x0420, 0x0421, 0x0422, 0x0423, 0x0424, 0x0425, 0x0426, 0x0427,
		0x0428, 0x0429, 0x042A, 0x042B, 0x042C, 0x042D, 0x042E, 0x042F,
		0x0430, 0x0431, 0x0432, 0x0433, 0x0434, 0x0435, 0x0436, 0x0437,
		0x0438, 0x0439, 0x043A, 0x043B, 0x043C, 0x043D, 0x043E, 0x043F,
		0x0440, 0x0441, 0x0442, 0x0443, 0x0444, 0x0445, 0x0446, 0x0447,
		0x0448, 0x0449, 0x044A, 0x044B, 0x044C, 0x044D, 0x044E, 0x044F
	};

	if(NULL == src || NULL == dst)
	{
		return 0;
	}

	size_t len = strlen((const char *)src);
	if(len >= ds_leght)
	{
		return 0;
	}

	memset((void *)dst, 0, ds_leght);

	const uint8_t *c = src;
	uint8_t *p = dst;

	uint_fast8_t i = 0;
	for (i = 0; i < len; i++)
	{
		uint16_t unicode = c[i] < 127u ? c[i] : win_cp1251[c[i] - 128u];
		p += unichar_to_utf8(unicode, p);
	}
	*p = 0;

	return 1u;
}

