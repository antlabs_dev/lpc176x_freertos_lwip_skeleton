/**
 * @file 	smtpclient.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Simple client to send E-Mails
 */

#ifndef SMTPCLIENT_H_
#define SMTPCLIENT_H_

#include "AppConfig.h"

/** How long to wait server's responce. Used in setsockopt() */
#define SMTP_BLOCKING_READ_TIMEOUT_SECONDS 10u

/** Defines a structure with all E-Mail message options */
struct s_email_message {
	const char *subj; //!< Subject of the message
	const char *message; //!< Message body
	const char *to_cc[8]; //!< First entry - To:, all other - copy. Last entry last be NULL terminated!
	const char *server; //!< SMTP server. Must support AUTH LOGIN without SSL
	const char *from_email; //!< Your sender email address
	const char *from_name; //!< Your name to be displayed as sender
	const char *login; //!< Your login on the server
	const char *password; //!< Your password on the server
};

/**
 * Send email message. Block until finish
 * @param message - pointer to struct s_email_message with all data
 * @return 1 if sent OK, 0 otherwise
 */
uint_fast8_t smtp_send(const struct s_email_message *mes);

#endif /* SMTPCLIENT_H_ */
