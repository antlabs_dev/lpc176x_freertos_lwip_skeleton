/**
 * @file 	cli_wrappers.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Implementation of commands for FreeRTOS+CLI
 */

#ifndef CLI_WRAPPERS_H_
#define CLI_WRAPPERS_H_

#include "AppConfig.h"

uint_fast8_t cli_wrappers_init(void);

/** This value returned from any command line process function should indicate that connection has to be cllosed */
#define CLI_WRAPPERS_REBOOT_CLOSE_CONNECTION_RETURN_FLAG (0xFF)

#endif /* CLI_WRAPPERS_H_ */
