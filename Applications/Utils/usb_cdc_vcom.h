/**
 * @file 	usb_cdc_vcom.h
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * USB virtual serial port @see original copyrigth in .c file.
 */

#ifndef USB_CDC_VCOM_H_
#define USB_CDC_VCOM_H_

#include "AppConfig.h"

/** Interrupt callback must be called from USB_IRQHandler() */
void USB_interrupt_callback(void);

/**
 * Send data via virtual serial port
 * @param buffer - pointer to buffer to send
 * @param size - size of data in bufer
 * @param ticks_to_block - ticks to wait on blockin send operation
 * @return 1 if data has been sent OK, 0 otherwise
 */
uint_fast8_t usb_vcom_send(const void *buffer, size_t size, portTickType ticks_to_block);

/**
 * Receive data via virtual serial port
 * @param buffer - pointer to buffer where data is being strored
 * @param size - size buffer (if received more then this size the data will be truncated)
 * @param ticks_to_block - ticks to wait on blockin receive for first byte (for others if any - wait 10 ticks)
 * @return size of data received
 */
size_t usb_vcom_recv(void *buffer,  size_t buffer_size, portTickType ticks_to_block);

#endif /* USB_CDC_VCOM_H_ */
