/**
 * @file 	iap.c
 * @author  Oleg Antonyan <oleg.b.antonyan@gmail.com>
 * @version 1.1
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * In-Application Programmin module for LPC176x
 */


#include "system_LPC17xx.h"
#include "LPC17xx.h"
#include "iap.h"

enum command_code {
	IAPCommand_Prepare_sector_for_write_operation = 50,
	IAPCommand_Copy_RAM_to_Flash,
	IAPCommand_Erase_sector,
	IAPCommand_Blank_check_sector,
	IAPCommand_Read_part_ID,
	IAPCommand_Read_Boot_Code_version,
	IAPCommand_Compare,
	IAPCommand_Reinvoke_ISP,
	IAPCommand_Read_device_serial_number
};

#define     FLASH_SECTOR_0       0x00000000
#define     FLASH_SECTOR_1       0x00001000
#define     FLASH_SECTOR_2       0x00002000
#define     FLASH_SECTOR_3       0x00003000
#define     FLASH_SECTOR_4       0x00004000
#define     FLASH_SECTOR_5       0x00005000
#define     FLASH_SECTOR_6       0x00006000
#define     FLASH_SECTOR_7       0x00007000
#define     FLASH_SECTOR_8       0x00008000
#define     FLASH_SECTOR_9       0x00009000
#define     FLASH_SECTOR_10      0x0000A000
#define     FLASH_SECTOR_11      0x0000B000
#define     FLASH_SECTOR_12      0x0000C000
#define     FLASH_SECTOR_13      0x0000D000
#define     FLASH_SECTOR_14      0x0000E000
#define     FLASH_SECTOR_15      0x0000F000
#define     FLASH_SECTOR_16      0x00010000
#define     FLASH_SECTOR_17      0x00018000
#define     FLASH_SECTOR_18      0x00020000
#define     FLASH_SECTOR_19      0x00028000
#define     FLASH_SECTOR_20      0x00030000
#define     FLASH_SECTOR_21      0x00038000
#define     FLASH_SECTOR_22      0x00040000
#define     FLASH_SECTOR_23      0x00048000
#define     FLASH_SECTOR_24      0x00050000
#define     FLASH_SECTOR_25      0x00058000
#define     FLASH_SECTOR_26      0x00060000
#define     FLASH_SECTOR_27      0x00068000
#define     FLASH_SECTOR_28      0x00070000
#define     FLASH_SECTOR_29      0x00078000

const char * const iap_sector_start_adress[] = {
    (char *)FLASH_SECTOR_0,
    (char *)FLASH_SECTOR_1,
    (char *)FLASH_SECTOR_2,
    (char *)FLASH_SECTOR_3,
    (char *)FLASH_SECTOR_4,
    (char *)FLASH_SECTOR_5,
    (char *)FLASH_SECTOR_6,
    (char *)FLASH_SECTOR_7,
    (char *)FLASH_SECTOR_8,
    (char *)FLASH_SECTOR_9,
    (char *)FLASH_SECTOR_10,
    (char *)FLASH_SECTOR_11,
    (char *)FLASH_SECTOR_12,
    (char *)FLASH_SECTOR_13,
    (char *)FLASH_SECTOR_14,
    (char *)FLASH_SECTOR_15,
    (char *)FLASH_SECTOR_16,
    (char *)FLASH_SECTOR_17,
    (char *)FLASH_SECTOR_18,
    (char *)FLASH_SECTOR_19,
    (char *)FLASH_SECTOR_20,
    (char *)FLASH_SECTOR_21,
    (char *)FLASH_SECTOR_22,
    (char *)FLASH_SECTOR_23,
    (char *)FLASH_SECTOR_24,
    (char *)FLASH_SECTOR_25,
    (char *)FLASH_SECTOR_26,
    (char *)FLASH_SECTOR_27,
    (char *)FLASH_SECTOR_28,
    (char *)FLASH_SECTOR_29
};

#define IAP_LOCATION 0x1FFF1FF1

typedef void (*IAP)(uint32_t [],uint32_t []);
IAP iap_entry;

/** Read chip's serial number into *d struct
 * Return error_code */
uint32_t iap_read_serial(uint32_t *id)
{
	iap_entry=(IAP)IAP_LOCATION;
	uint32_t command[5]={0,0,0,0,0};
	uint32_t result[5]={0,0,0,0,0};
	command[0]=IAPCommand_Read_device_serial_number;
	iap_entry(command,result);
	id[0] = result[1];
	id[1] = result[2];
	id[2] = result[3];
	id[3] = result[4];
	return result[0];
}

/** Read chip's ID into *d integer
 *  Return: error_code
 */
uint32_t iap_read_id(uint32_t *d)
{
	iap_entry=(IAP)IAP_LOCATION;
	uint32_t command[5]={0,0,0,0,0};
	uint32_t result[5]={0,0,0,0,0};
	command[0]=IAPCommand_Read_part_ID;
	iap_entry(command,result);
	*d=result[1];
	return result[0];
}

/** Prepare specified sector(s) number(s) from start to end to write/erase operations
 * start <= end
 * Return: error_code
*/
uint32_t iap_write_prepare(uint32_t start, uint32_t end)
{
	iap_entry=(IAP)IAP_LOCATION;
	uint32_t command[5]={0,0,0,0,0};
	uint32_t result[5]={0,0,0,0,0};
	command[0]= IAPCommand_Prepare_sector_for_write_operation;
	command[1]= start;
	command[2]= end;
	iap_entry(command,result);
	return result[0];
}

/** Write data from src (RAM) to dst (FLASH). dst should be absolute address
 * i.e. sector_start_adress[16] : write to 16 sector with 0 offset
 * sector_start_adress[16] + 256 : write to 16 sector with 256 bytes offset
 * offset must be 256 | 512 | 1024 | 4096
 * Sector must be prepared to write by iap_write_prepare() function
 * Return: error_code
 */
uint32_t iap_write(const uint8_t *src, const uint8_t *dst, size_t size)
{
	iap_entry=(IAP)IAP_LOCATION;
	uint32_t command[5]={0,0,0,0,0};
	uint32_t result[5]={0,0,0,0,0};
	command[0]= IAPCommand_Copy_RAM_to_Flash;
	command[1]= (uint32_t)dst;    //  Destination flash address where data bytes are to be written. This address should be a 256 byte boundary.
	command[2]= (uint32_t)src;    //  Source RAM address from which data bytes are to be read. This address should be a word boundary.
	command[3]= size;                  //  Number of bytes to be written. Should be 256 | 512 | 1024 | 4096.
	command[4]= SystemCoreClock/1000;	// CPU clock in kHz
	iap_entry(command,result);
	return result[0];
}

/** Check if sector(s) from start to end are blank
 * start <= end
 * Return: error_code
 */
uint32_t iap_blank_check(uint32_t start, uint32_t end)
{
	iap_entry=(IAP)IAP_LOCATION;
	uint32_t command[5]={0,0,0,0,0};
	uint32_t result[5]={0,0,0,0,0};
    command[0]= IAPCommand_Blank_check_sector;
    command[1]= start;  //  Start Sector Number
    command[2]= end;    //  End Sector Number (should be greater than or equal to start sector number)
    iap_entry(command, result);
    return result[0];
}

/** Erase specified sectors(s) from start to end
 * start <= end
 * Return: error_code
 */
uint32_t iap_erase(uint32_t start, uint32_t end)
{
	iap_entry=(IAP)IAP_LOCATION;
	uint32_t command[5]={0,0,0,0,0};
	uint32_t result[5]={0,0,0,0,0};
    command[0]= IAPCommand_Erase_sector;
    command[1]= start;  //  Start Sector Number
    command[2]= end;    //  End Sector Number (should be greater than or equal to start sector number)
    command[3]= SystemCoreClock/1000;            //  CPU Clock Frequency (CCLK) in kHz
    iap_entry(command, result);
    return result[0];
}

/** Compare size data from src with dst
 * Return: error_code
 */
uint32_t iap_compare(const uint8_t *src, const uint8_t *dst, size_t size)
{
	iap_entry=(IAP)IAP_LOCATION;
	uint32_t command[5]={0,0,0,0,0};
	uint32_t result[5]={0,0,0,0,0};
    command[0]= IAPCommand_Compare;
    command[1]= (uint32_t)dst;    //  Starting flash or RAM address of data bytes to be compared. This address should be a word boundary.
    command[2]= (uint32_t)src;    //  Starting flash or RAM address of data bytes to be compared. This address should be a word boundary.
    command[3]= size;                         //  Number of bytes to be compared; should be a multiple of 4.
    iap_entry(command, result);
    return result[0];
}

/** Enter ISP mode programmatically
 */
void iap_reinvoke_isp(void)
{
	__disable_irq(); 		// good practice to disable before feeding
	LPC_SC->PLL0CON = 0x1; // disconnect PLL0
	LPC_SC->PLL0FEED = 0xAA;
	LPC_SC->PLL0FEED = 0x55;
	while (LPC_SC->PLL0STAT&(1<<25));
	LPC_SC->PLL0CON = 0x0;    // power down
	LPC_SC->PLL0FEED = 0xAA;
	LPC_SC->PLL0FEED = 0x55;
	while (LPC_SC->PLL0STAT&(1<<24));
	LPC_SC->FLASHCFG &= 0x0fff;  // This is the default flash read/write setting for IRC
	LPC_SC->FLASHCFG |= 0x5000;
	LPC_SC->CCLKCFG = 0x0;     //  Select the IRC as clk
	LPC_SC->CLKSRCSEL = 0x00;
	LPC_SC->SCS = 0x00;		    // not using XTAL anymore

	iap_entry=(IAP)IAP_LOCATION;
	uint32_t command[5]={0,0,0,0,0};
	uint32_t result[5]={0,0,0,0,0};
	command[0]=IAPCommand_Reinvoke_ISP;
	iap_entry(command, result);
}
